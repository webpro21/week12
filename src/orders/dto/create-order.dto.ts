import { IsNotEmpty, Length } from 'class-validator';
class CreateOrderTtemDto {
  @IsNotEmpty()
  productId: number;
  @IsNotEmpty()
  amount: number;
}
export class CreateOrderDto {
  @IsNotEmpty()
  cusstomerId: number;
  @IsNotEmpty()
  orderItem: CreateOrderTtemDto[];
}
